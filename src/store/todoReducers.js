import {
  CHANGE_INPUT_VALUE,
  ADD_LIST_ITEM,
  INIT_TODO_LIST,
} from "./actionTypes";
const defaultState = {
  inputValue: "",
  list: [],
};

const reducer = (state = defaultState, action) => {
  if (action.type === CHANGE_INPUT_VALUE) {
    return {
      ...state,
      inputValue: action.value,
    };
  }
  if (action.type === ADD_LIST_ITEM) {
    return {
      ...state,
      list: [
        ...state.list,
        { id: (Math.random() * 100).toFixed(0), name: state.inputValue },
      ],
    };
  }
  if (action.type === INIT_TODO_LIST) {
    console.log(action);
    return {
      ...state,
      list: action.payload,
    };
  }
  return state;
};

export default reducer;
