export const CHANGE_INPUT_VALUE = "change_input_value";
export const ADD_LIST_ITEM = "add_list_item";
export const FETCH_LIST_DATA = "fetch_list_data";
export const INIT_TODO_LIST = "init_todo_list";
