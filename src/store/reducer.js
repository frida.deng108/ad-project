import { combineReducers } from "redux";
import todoReducers from "./todoReducers";
import headerReducers from "./headerReducers";

const rootReducers = combineReducers({
  todos: todoReducers,
  header: headerReducers,
});

export default rootReducers;
