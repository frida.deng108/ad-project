import {
  CHANGE_INPUT_VALUE,
  ADD_LIST_ITEM,
  INIT_TODO_LIST,
  FETCH_LIST_DATA,
} from "./actionTypes";
import axios from "axios";

export const changeInputValue = (value) => {
  return {
    type: CHANGE_INPUT_VALUE,
    value,
  };
};

export const addListItem = () => {
  return {
    type: ADD_LIST_ITEM,
  };
};

// export const initTodoList = (data) => {
//   return {
//     type: INIT_TODO_LIST,
//     data,
//   };
// };

export const getListData = () => {
  return (dispatch) => {
    // dispatch({ type: FETCH_LIST_DATA });
    axios
      .get("/api/getData")
      .then((response) => {
        console.log(response);

        dispatch({ type: INIT_TODO_LIST, payload: response.data.data });
      })
      .catch((error) => {
        // dispatch({ type: "FETCH_USER_FAILURE", payload: error.message });
      });
  };
};
