import Mock from "mockjs";

// 配置 MockJS
Mock.setup({
  timeout: "200-600",
});

// 拦截 Axios 请求并返回 Mock 数据
Mock.mock("/api/getData", "get", {
  status: "success",
  message: "数据获取成功",
  "data|1-10": [
    {
      "id|+1": 1,
      name: "@cname",
      "age|18-60": 1,
      "gender|1": ["男", "女"],
      email: "@EMAIL",
      birthday: '@date("yyyy-MM-dd")',
    },
  ],
});
Mock.mock("/api/todo/getData", "get", {
  status: "success",
  message: "数据获取成功",
  "data|1-10": [
    {
      "id|+1": 1,
      name: "@cname",
    },
  ],
});

Mock.mock("/api/postData", "post", {
  status: "success",
  message: "数据提交成功",
});
