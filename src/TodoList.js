import React, { Component } from "react";
import { Input, List, Button } from "antd";
import store from "./store";
import { changeInputValue, addListItem, getListData } from "./store/action";
import { connect } from "react-redux";

class TodoList extends Component {
  constructor(props) {
    super(props);
    // this.handleInputChange = this.handleInputChange.bind(this);
    // this.handleAddItem = this.handleAddItem.bind(this);
  }
  componentDidMount() {
    this.props.fetchListData();
    // store.subscribe(() => {
    //   this.setState(store.getState());
    // });
  }
  //   handleInputChange(e) {
  //     console.log(e.target.value);
  //     const action = changeInputValue(e.target.value);
  //     store.dispatch(action);
  //   }
  //   handleAddItem() {
  //     const action = addListItem();
  //     store.dispatch(action);
  //   }
  //   fetchListData = () => {
  //     const action = getListData();
  //     store.dispatch(action);
  //   };

  render() {
    const { inputValue, list, handleAddItem, handleInputChange } = this.props;
    return (
      <div>
        <Input
          placeholder="Basic usage"
          value={inputValue}
          onChange={handleInputChange}
        />
        <Button type="primary" onClick={handleAddItem}>
          提交
        </Button>
        <List
          header={<div>Header</div>}
          footer={<div>Footer</div>}
          bordered
          dataSource={list}
          renderItem={(item) => (
            <List.Item key={item.id}>{item.name}</List.Item>
          )}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ todos }) => {
  return { ...todos };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleAddItem: () => dispatch(addListItem()),
    handleInputChange: (e) => dispatch(changeInputValue(e.target.value)),
    fetchListData: () => dispatch(getListData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
